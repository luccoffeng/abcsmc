# Bootstrap distance for terminal tolerance -----
#' Bootstrap the distance metric(s) to inform the desired final tolerance level
#'
#' The \code{bootstrap_e} function performs a bootstrap of the observed data to
#' determine a reasonable value for the tolerance levels in the ABC-SMC
#' procedure.
#'
#' @param y_obs Object holding the observed data. Must be a vector, data.frame,
#'   or data.table.
#' @param fun_distance Function to calculate the distance between the observed
#'   and bootstrapped data. The function should return either a scalar or
#'   a vector with the number of elements.
#' @param n_rep Integer number of bootstrap iterations.
#' @param p Scalar indicating the percentile of bootstrapped values to be
#'   returned. Values in the range of 0.25 to 0.5 seem to work well; using lower
#'   values may result in extremely long computation times for some ABC-SMC
#'   exercises, depending on the data and distance function. Therefore, may sure
#'   to explore before running a full "final" ABC-SMC analysis.
#' @param ... Arguments passed down to \code{fun_distance()}.
#'
#' @return A scalar or vector containing the \code{p}-th percentile of
#'   bootstrapped distances. Values are only valid for ABC-SMC procedures that
#'   use the exact same \code{y_obs} and \code{fun_distance}.
#'
#' @export
bootstrap_e <- function(y_obs,
                        fun_distance,
                        n_rep = 1e4,
                        p = .25,
                        ...) {

  bootstrap_data <- NULL

  if (is.vector(y_obs)) {
    bootstrap_data <- lapply(1:n_rep, function(i) {
      sample(y_obs, replace = TRUE)
    })
  }
  if (is.data.frame(y_obs) | is.data.table(y_obs)) {
    bootstrap_data <- lapply(1:n_rep, function(i) {
      y_obs[sample(nrow(y_obs), replace = TRUE), ]
    })
  }

  if (is.null(bootstrap_data))
    stop("y_obs must be a vector, data.frame, or data.table")

  D <- fun_distance(bootstrap_data,
                    y_obs,
                    ...)

  apply(D, 2, quantile, prob = p, type = 1)

}


# Transformation of a parameter with lower and/or upper bound -----
#' Tranformation of a parameter with lower and/or upper bound
#'
#' The \code{transform_theta} function transforms values on the natural scale to
#' the unconstrained scale, using either the natural logarithm (one bound) or
#' the extended logistic function. The function can alternatively return the
#' Jacobian, which is the absolute value of the first derivative of the
#' transformation from the constrained to the unconstrained scale. Optionally,
#' it returns the inverse transform (Jacobian not available for this). If the
#' bounds of the parameter space are (-Inf,Inf), \code{transform_theta} returns
#' the input values, or 1 in case of the Jacobian. Jacobian are calculated
#' univariately, as all the transformations are univariate as well (i.e., the
#' determinant of the matrix of partial derivatives only involves the diagonal
#' elements as off-diagonal partial derivatives are all zero).
#'
#' @param x A numeric vector or matrix of parameter values.
#' @param bound_lo Lower bound.
#' @param bound_hi Upper bound.
#' @param inverse Logical flag indicating whether the inverse transformation
#'   should be performed.
#' @param jacobian Logical flag indicating whether the absolute of the first
#'   derivative (or its inverse) should be returned for the transformation from
#'   the constrained to the unconstrained scale.
#' @param log Logical flag indicating whether the logarithm of the Jacobian
#'   should be returned (if it is calculated).
#'
#' @return A numeric vector or matrix of the same dimensions as argument
#'   \code{x}.
#'
#' @export
transform_theta <- function(x,
                            bound_lo = -Inf,
                            bound_hi = Inf,
                            inverse = FALSE,
                            jacobian = FALSE,
                            log = FALSE) {

  if (inverse & jacobian) stop("Absolute derivative only available for transformation from the constrained to unconstrained scale")
  if (bound_lo >= bound_hi) stop("'min' must be less than 'max'")

  A <- bound_lo
  B <- bound_hi

  if (jacobian) {

    # Calculate first derivative of transformation to the unconstrained scale
    if (A == -Inf && B == Inf) {
      y <- rep(1, length(x))
    }

    if (A > -Inf && B < Inf) {
      y <- (B - A) / ((x - A) * (B - x))
    }

    if (A == -Inf && B < Inf) {
      y <- -1 / (B - x)
    }

    if (A > -Inf && B == Inf) {
      y <- 1 / (x - A)
    }

    # Return absolute value
    if (log) log(abs(y)) else abs(y)

  } else {

    if (inverse) {

      if (A == -Inf && B == Inf) {
        y <- x
      }

      if (A > -Inf && B < Inf) {
        exp_x <- exp(x)
        y <- (A + B * exp_x) / (1 + exp_x)
      }

      if (A == -Inf && B < Inf) {
        y <- B - exp(x)
      }

      if (A > -Inf && B == Inf) {
        y <- A + exp(x)
      }

    } else {

      if (A == -Inf && B == Inf) {
        y <- x
      }

      if (A > -Inf && B < Inf) {
        y <- log((x - A)/(B - x))
      }

      if (A == -Inf && B < Inf) {
        y <- log(B - x)
      }

      if (A > -Inf && B == Inf) {
        y <- log(x - A)
      }

    }

    y

  }

}


# Calculate acceptance rate ----
#' Calculate acceptance rate
#'
#' The \code{calc_accept} function returns the acceptance rate, given distances
#' and tolerance.
#'
#' @param D A list of matrices containing distances as produced by
#'   \code{\link{sim_rep_data}}.
#' @param tol Tolerance level of the current SMC iteration.
#' @param ... Optional arguments passed down from higher level functions.
#'
#' @return A numeric vector with acceptance probability per particle.
#'
#' @export
calc_accept <- function(D, tol, ...) {

  temp <- do.call(rbind, lapply(D, function(x) {
    apply(t(x) <= tol & t(x) < Inf, 2, all)
  }))

  apply(temp, 2, mean, na.rm = TRUE)

}


# Prepare prior parameters ----
#' Prepare prior parameters
#'
#' The \code{prep_prior} function check whether lower and upper bounds are
#' defined for each paramater, and if not, adds bounds at -Inf and/or Inf.
#'
#' @param prior_par See \code{\link{rprior}}.
#'
#' @export
prep_prior <- function(prior_par) {
  lapply(prior_par, function(x) {

    if (!any("bound_lo" == names(x))) x$bound_lo <- -Inf
    if (!any("bound_hi" == names(x))) x$bound_hi <- Inf

    if (with(x, bound_lo >= bound_hi))
      stop(paste0("At least one of the priors has a lower bound that is higher than the upper bound"))

    x

  })
}


# Check presence of required function arguments
check_args <- function(function_name, required_input, input_present) {
  if (any(!input_present))
    stop(paste0(function_name,
                " arguments missing: ",
                paste0(required_input[!input_present], collapse = ", ")))
}


# Calculate Mahalonobis distance ----
#' Calculate Mahalonobis distance
#'
#' The \code{calc_Mahal} calculates the Mahalonobis distance between two sets of
#' multivariate parameter values (samples) in an efficient way.
#'
#' @param x A numeric matrix with one row per sample of multivariate parameters.
#' @param y A numeric matrix with one row per sample of multivariate parameters.
#' @param Sigma Covariance matrix with dimensions equal to the number of columns
#'   of x and y.
#'
#' @export
Mahal_pairwise <- function(x, y, Sigma) {
  # This function calculates the pairwise Mahalanobis distances. See
  # https://stats.stackexchange.com/questions/65705/pairwise-mahalanobis-distances/81710#81710
  # for documentation.

  # Calculate inverse square root matrix
  if(nrow(Sigma) > 1){
    invCov <- solve(Sigma)
    svds <- svd(invCov)
    invCovSqr <- svds$u %*% diag(sqrt(svds$d)) %*% t(svds$u)
  }else{
    invCovSqr <- 1 / sqrt(Sigma)
  }

  # Transform the coordinates
  xQ <- x %*% invCovSqr
  yQ <- y %*% invCovSqr

  # Calculate distances by summing the squared distance over dimensions
  sqrDiffs <- 0
  for(i in 1:ncol(x)){
    sqrDiffs <- sqrDiffs + outer(xQ[, i], yQ[, i], "-")^2
  }
  sqrDiffs
}

# Calculate multivariate normal density ----
#' Calculate multivariate normal density
#'
#' The \code{calc_dmvn} calculates the multivariate normal density for all
#' possible combinations of rows from two matrices, given a covariance matrix.
#'
#' @param x A numeric matrix with one row per sample of multivariate parameters.
#' @param y A numeric matrix with one row per sample of multivariate parameters.
#' @param Sigma Covariance matrix with dimensions equal to the number of columns
#'   of x and y.
#'
#' @export
dmvn_pairwise <- function(x, y, Sigma) {
  constant <- (2 * pi)^(-nrow(Sigma) / 2) / sqrt(det(Sigma))
  return(constant * exp(-.5 * Mahal_pairwise(x, y, Sigma)))
}


# Weighted variance
#' Calculate weighted variance
#'
#' The \code{weighted_var} function calculates the weighted variance.
#'
#' @param x A numeric vector
#' @param w A numeric vector of weights
#' @param na_rm Logical flag indicating whether missing values should be
#'   removed.
#'
#' @export
weighted_var <- function(x, w, na_rm = FALSE) {
  if(na_rm) {
    i <- !is.na(x)
    w <- w[i]
    x <- x[i]
  }
  sum_w <- sum(w)
  (sum(w*x^2) * sum_w - sum(w*x)^2) / (sum_w^2 - sum(w^2))
}


# Check for nonsensical distances ----
#' Check for nonsensical distances
#'
#' The \code{check_distances} function checks that distances or non-NaN and
#' greater than zero (returns FALSE if either is not the case).
#'
#' @param x A SMC iteration object.
#'
#' @export
check_distances_ok <- function(x) {
  d_temp <- unlist(x$distance)
  !any(is.na(d_temp) | d_temp < 0)
}


### END OF CODE ### ----
