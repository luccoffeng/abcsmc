
# abcsmc

<!-- badges: start -->
<!-- badges: end -->

The goal of abcsmc is to provide a high-level user-friendly interface to perform approximate Bayesian computation based on sequantial Monte Carlo

## Installation

You can install the latest released version of abcsmc from GitLab:

``` r
remotes::install_gitlab("luccoffeng/abcsmc")
```

The development version can be downloaded by:

``` r
remotes::install_gitlab("luccoffeng/abcsmc@dev")
```


## Example 1: normal distribution (unknown mean and standard deviation)

This is a basic example which shows how to run the two-parameter normal model:

``` r
library(abcsmc)
library(ggpolypath)  # need to load manually to avoid bug in ggpolypath package

# The functions norm2p_model, norm2p_prior, norm2p_distance, and
# norm2p_distance_1d come with the abcsmc package and together represent a
# two-parameter model of a normal distribution with unknown mean and standard
# deviation. The distance between the observed and simulated data can be either
# based on a single distance metric (the norm2p_distance_1d function) or two
# distance metrics (the norm2p_distance function).

# Generate a parameter set and generate data set from it
my_param <- rprior(size = 1, prior_par = norm2p_prior)
my_n_data <- 50
my_data <- norm2p_model(theta = my_param, N_data = my_n_data)[[1]]

# Determine appropriate terminal tolerance level
my_e_term_2d <- bootstrap_e(y_obs = my_data,
                            fun_distance = norm2p_distance,
                            n_rep = 1e4,
                            p = .25)

# Fit model based on two distance metrics
fit_2p_2d <- smc(fun_gen_data = norm2p_model,
                 fun_distance = norm2p_distance,
                 prior_par = norm2p_prior,
                 N_p = 250,
                 y_obs = my_data,
                 e_term = my_e_term_2d,
                 N_data = length(my_data))

summarise_SMC(fit_2p_2d)  # prints a summary of the fit
plot_evolution(fit_2p_2d, par = "mu")  # plot the evolution of parameter 'mu'
plot_evolution(fit_2p_2d, par = "sigma")  # plot the evolution of parameter 'sigma'
plot_wcdf(fit_2p_2d, par = "sigma")  # plot the weighted CDF
plot_2d_post(fit_2p_2d[[length(fit_2p_2d)]],
             par_names = c("mu", "sigma"))  # plot the final joint posterior

# Fit model based on one distance metrics (redo bootstrap)
my_e_term_1d <- bootstrap_e(y_obs = my_data,
                            fun_distance = norm2p_distance_1d,
                            n_rep = 1e4,
                            p = .25)
fit_2p_1d <- smc(fun_gen_data = norm2p_model,
                 fun_distance = norm2p_distance_1d,
                 prior_par = norm2p_prior,
                 N_p = 250,
                 y_obs = my_data,
                 e_term = my_e_term_1d,
                 N_data = length(my_data),
                 save_sim_data = TRUE)  # saves the replicate data

summarise_SMC(fit_2p_1d)
plot_evolution(fit_2p_1d, par = "mu")
plot_evolution(fit_2p_1d, par = "sigma", log_axis = TRUE)

plot_wcdf(fit_2p_1d, par = "mu", log_axis = FALSE)
plot_wcdf(fit_2p_1d, par = "sigma", log_axis = FALSE)

plot_2d_post(fit_2p_1d[[length(fit_2p_1d)]],
             par_names = c("mu", "sigma"))


# Compare the fits based on 1D and 2D distance metrics in terms of the 2D
# distances between the observed and simualted data (i.e., distances are
# recalculated for the 1D fit)
distance_alt <-
do.call(rbind,
        lapply(1:length(rev(fit_2p_1d)[[1]]$data_rep),
               function(i) {
                 norm2p_distance(rev(fit_2p_1d)[[1]]$data_rep[[i]],
                                 y_obs = my_data)
                           }))
plot(distance_alt)  # Triangular area accepted
plot(do.call(rbind, rev(fit_2p_2d)[[1]]$distance))  # Square area accepted

```

## Example 2: negative binomial distribution (unknown mean and shape)

This is a basic example which shows how to run the two-parameter NB model:

``` r

# Test the negative binomial toy model
my_param <- rprior(size = 1, prior_par = nb_prior)
my_n_data <- 500
my_data <- nb_model(theta = my_param, N_data = my_n_data)[[1]]
my_quantiles <- 1:39 * .025
my_e_term <- bootstrap_e(y_obs = my_data,
                         fun_distance = nb_distance_1d,
                         n_rep = 1e4,
                         p = .25,
                         nb_quantiles = my_quantiles)

fit_nb <- smc(
  N_p = 250,
  e_term = my_e_term,
  e_perc = 0.5,
  parallel = FALSE,
  save_temp = FALSE,
  kernel_type = 1,
  fun_gen_data = nb_model,
  fun_distance = nb_distance_1d,
  prior_par = nb_prior,
  y_obs = my_data,
  N_data = my_n_data,          # argument for the fun_gen_data function
  nb_quantiles = my_quantiles  # argument for the fun_distance function
)

# Continue the ABC-SMC where it left of, but with a more stringent terminal
# tolerance, again based on bootstrapping. This will cost considerably more time
# and may be expected to gain not much more precision.
my_e_term_extra <- bootstrap_e(y_obs = my_data,
                               fun_distance = nb_distance_1d,
                               n_rep = 1e4,
                               p = .025,
                               nb_quantiles = my_quantiles)

fit_nb_extra <- smc(
  fit = fit_nb,
  N_p = 250,
  e_term = my_e_term_extra,
  e_perc = 0.5,
  parallel = FALSE,
  save_temp = FALSE,
  kernel_type = 1,
  fun_gen_data = nb_model,
  fun_distance = nb_distance_1d,
  prior_par = nb_prior,
  y_obs = my_data,
  N_data = my_n_data,          # argument for the fun_gen_data function
  nb_quantiles = my_quantiles  # argument for the fun_distance function
)

summarise_SMC(fit_nb)
summarise_SMC(fit_nb_extra)[length(fit_nb_extra),]
plot_evolution(fit_nb_extra, par = "mu", log_axis = TRUE)
plot_evolution(fit_nb_extra, par = "k", log_axis = TRUE)
plot_wcdf(fit_nb, par = "mu", log_axis = TRUE)
plot_wcdf(fit_nb_extra[-c(1:length(fit_nb))], par = "mu", log_axis = TRUE)

```

## Example 3: binomial distribution (unknown mean)

This is a basic example which shows how to run the one-parameter binomial model:

``` r

# Test the binomial toy model
my_param <- rprior(size = 1, prior_par = binom_prior)
my_n_data <- 50
my_data <- binom_model(theta = my_param, N_data = my_n_data)[[1]]

# The bootstrap for terminal tolerance needs to be done a little more
# thoughtfully, and let's calculate two alternative terminal tolerances at the
# same time (to be used one at a time each)
my_e_term <-
  bootstrap_e(y_obs = c(rep(1, my_data), rep(0, my_n_data - my_data)),
              fun_distance = function(y_rep, y_obs, ...)
                               binom_distance(lapply(y_rep, sum),
                                              sum(y_obs)),
              n_rep = 1e4,
              p = c(.025, .25))

fit_binom <- smc(
  N_p = 250,
  e_term = my_e_term[2],
  e_perc = 0.5,
  parallel = FALSE,
  save_temp = FALSE,
  kernel_type = 1,
  fun_gen_data = binom_model,
  fun_distance = binom_distance,
  prior_par = binom_prior,
  y_obs = my_data,
  N_data = my_n_data  # argument for the fun_gen_data function
)

# Continue the ABC-SMC where it left off, but with a more stringent terminal
# tolerance, again based on bootstrapping. This will cost some more time
# (not so much for this particular model) and may be expected to gain not much
# more precision.
fit_binom_extra <- smc(
  fit = fit_binom,
  N_p = 250,
  e_term = my_e_term[1],
  e_perc = 0.5,
  parallel = FALSE,
  save_temp = FALSE,
  kernel_type = 1,
  fun_gen_data = binom_model,
  fun_distance = binom_distance,
  prior_par = binom_prior,
  y_obs = my_data,
  N_data = my_n_data  # argument for the fun_gen_data function
)

summarise_SMC(fit_binom)
summarise_SMC(fit_binom_extra)[length(fit_binom_extra),]

plot_evolution(fit_binom_extra, par = "mu")
plot_wcdf(fit_binom_extra, par = "mu")

```
